var app = angular.module('StudentIDs',[
	'ngRoute'
	]);

app.config(function($locationProvider, $routeProvider){
	$locationProvider.hashPrefix('');

	$routeProvider
	.when('/',{
		templateUrl: 'partials/main.html'
	})
	.when('/about',{
		templateUrl: 'partials/about.html'
	})
	.when('/new',{
		templateUrl: 'partials/new.html'
	});
});

app.controller('studentIDsController', function($location, $scope){
	$scope.studentIDsList = [
		{
			Student:{
				name: 'Jonaitis, Jonas',
				personalCode: 123456789
			},
			cardNumber: 100,
			status: 'Neaktyvi'
		},
		{
			Student:{
				name: 'Antanaitis, Antanas',
				personalCode: 987654321
			},
			cardNumber: 101,
			status: 'Aktyvi'
		},
		{
			Student:{
				name: 'Pranaitis, Pranas',
				personalCode: 135792468
			},
			cardNumber: 102,
			status: 'Neaktyvi'
		},
		{
			Student:{
				name: 'Onaityte, Ona',
				personalCode: 246813579
			},
			cardNumber: 103,
			status: 'Aktyvi'
		},
		{
			Student:{
				name: 'Gedaitis, Gedas',
				personalCode: 864297531
			},
			cardNumber: 104,
			status: 'Aktyvi'
		}
	];

	$scope.studentIDsStatus = 'Aktyvi';
	$scope.listTitle = 'Aktyvūs studentų pažymėjimai';

	$scope.student = {};

	$scope.createNewStudentID = function(){
		$scope.studentIDsList.push({
			Student:{
				name: $scope.student.surname + ', ' + $scope.student.name,
				personalCode: $scope.student.personalCode
			},
			cardNumber: $scope.studentIDsList[$scope.studentIDsList.length - 1].cardNumber + 1,
			status: 'Neaktyvi'
		});

		$scope.student = {};
		$location.path('/');
	}

	$scope.cancel = function(){
		$scope.student = {};
		$location.path('/');
	};

	$scope.getStudentIDs = function(status) {
		$scope.studentIDsStatus = status;

		if (status == 'Aktyvi') {
			$scope.listTitle = 'Aktyvūs studentų pažymėjimai';
		}
		else {
			$scope.listTitle = 'Neaktyvūs studentų pažymėjimai';
		}
	}

	$scope.getStudentIDByCardNumber = function(cardNr){
		var studentID = $scope.studentIDsList.find(x => x.cardNumber === cardNr);
		return studentID;
	};

	$scope.deleteStudentID = function(cardNr){
		var studentID = $scope.getStudentIDByCardNumber(cardNr);
		var index = $scope.studentIDsList.indexOf(studentID);

		var result = confirm("Ar tikrai norite pašalinti studento pažymėjimą, priklausantį " + studentID.Student.name + " (a.k. " + studentID.Student.personalCode + ")?");
		if(result){
			$scope.studentIDsList.splice(index, 1);
		}
	};

	$scope.changeStudentIDStatus = function(cardNr){
		var studentID = $scope.getStudentIDByCardNumber(cardNr);
		studentID.status = (studentID.status == "Aktyvi")? "Neaktyvi" : "Aktyvi";

		alert("Studento " + studentID.Student.name + " (a.k. " + studentID.Student.personalCode + ") pažymėjimo būsena sėkmingai pakeista į '" + studentID.status + "'.");
	};
});